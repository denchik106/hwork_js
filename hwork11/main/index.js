//1. Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища

const btn = document.querySelector(".btn");
const divUsers = document.querySelector("#div");

btn.addEventListener(`click`, function () {
    const divUser = document.createElement("p");

    class User {
        constructor(named, snamed) {
            this.name = named;
            this.sname = snamed;
        };
        derivation() {
            alert(`Привет, ${this.name} ${this.sname}`)

        }
    };

    function named() {
        let names = prompt("Введите ваше имя");
        while (names === null || names === "") {
            alert('Введите ваше имя !!!');
            names = prompt(`Введите имя`);
        }
        return names;
    };

    function snamed() {
        let snames = prompt("Введите вашу фамилию");
        while (snames === null || snames === "") {
            alert('Введите вашу фамилию!!!');
            snames = prompt(`Введите фамилию`);
        }
        return snames;
    }

    let users = new User(
        named(), snamed());

    users.derivation();
    divUsers.appendChild(divUser);
    divUser.innerText = (`Имя пользователя: ${users.name}
      Фамилия пользователя: ${users.sname}`)

});


//2.Створи список, що складається з 4 аркушів.
//Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний

const li2 = document.getElementsByTagName("li")[1];

li2.nextElementSibling.style.background = "blue";

li2.previousElementSibling.style.background = "red";



//3.Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки


const idv = document.querySelector(".div1");
let idv2 = document.querySelector(".div2");

idv.onmouseover = function (e) {
    document.querySelector(".div3").innerText = (`X: ${e.clientX}  Y: ${e.clientY}`)
};

idv.onmousemove = function (e) {
    let left = e.offsetX + 5;
    let top = e.offsetY + 5;
    idv2.innerHTML = "X = " + top + "<br>" + " Y = " + left;
    idv2.style.top = top + "px";
    idv2.style.left = left + "px";
};

idv.onmouseout = function (e) {
    idv2.style.top = "50%";
    idv2.style.left = "50%";
    idv2.innerHTML = `Hello!`;
};


//4.Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута.


let box = document.createElement("div");
document.body.append(box);

function el() {
    for (let i = 1; i < 6; i++) {
        let btn3 = document.createElement("input");
        btn3.type = "button";
        btn3.classList.add("number");
        btn3.style.display = "block";
        btn3.style.width = "50px";
        btn3.value = i;
        box.appendChild(btn3);
    };
};

el();

const [...button] = document.querySelectorAll(".number");

button.forEach(element => {
    element.addEventListener("click", () => {
        let inp = document.createElement("p");
        box.appendChild(inp);
        inp.innerText = ` Нажата кнопка: ${element.value}`;

    }
    )

})


//5.Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці


const changeDiv = document.createElement("div");
changeDiv.classList.add("div99");
document.body.append(changeDiv);

const random = (min, max) => {
    const rand = min + Math.random() * (max - min + 1);
    return Math.floor(rand);
};

const btn12 = document.querySelector(`.div99`);

btn12.addEventListener('mouseenter', () => {
    btn12.style.left = `${random(0, 90)}%`;
    btn12.style.top = `${random(0, 90)}%`;
});


btn12.addEventListener(`click`, () => {
    alert(`Победа !!! :D`);
});


//6.Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body

let color = document.createElement("input");
color.id = "color"
color.type = "color";
color.style.marginTop = "60px";
document.body.append(color);

let btn9 = document.createElement("input");
btn9.type = "button"
btn9.value = "Залить страницу";
document.body.append(btn9);

btn9.addEventListener("click", function (event) {
    document.body.style.background = color.value;
})


//7.Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль

let consoleInput = document.createElement("input");
consoleInput.style.display = "block";
consoleInput.type = "text";
document.body.append(consoleInput);

consoleInput.addEventListener("input", function (e) {
    console.log(this.value);
}, false)


//8.Створіть поле для введення даних у полі введення даних виведіть текст під полем


const value = document.createElement(`input`);
value.id = "value";
value.placeholder = "Hello"
document.body.append(value);

const rez = document.querySelector("#value");

rez.addEventListener("change", () => {
    let rezult = document.createElement("div");
    document.body.append(rezult);
    rezult.innerText = rez.value;
})







