import React from "react";
import ReactDom from "react-dom";


const App = function () {
    return (
        <div>
            <Month></Month>
            <Week></Week>
            <Sign></Sign>
        </div>
    )
};

const color = {
    color: `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`,
    fontSize: "20px",
    lineHeight: "24px",
    margin: "10px"
};

const color1 = {
    color: `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`,
    fontSize: "20px",
    lineHeight: "24px",
    margin: "10px"
};

const color2 = {
    color: `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`,
    fontSize: "20px",
    lineHeight: "24px",
    margin: "10px"
};

const ulStyle = {
    listStyle: "none",
    display: "flex",
    width: "100%",
    justifyContent: "space-around",
    paddingInlineStart: "0px"
};

const Month = function () {
    return (
        <div>
            <h1>Месяца года :</h1>
            <ul style={ulStyle}>
                <li style={color}>Январь</li>
                <li style={color1}>Февраль</li>
                <li style={color2}>Март</li>
                <li style={color}>Апрель</li>
                <li style={color1}>Май</li>
                <li style={color2}>Июнь</li>
                <li style={color}>Июль</li>
                <li style={color1}>Август</li>
                <li style={color2}>Сентябрь</li>
                <li style={color}>Октябрь</li>
                <li style={color1}>Ноябрь</li>
                <li style={color2}>Декабрь</li>
            </ul>
        </div>)
};


const Week = () => {
    return (
        <div>
            <h2>Дни недели :</h2>
            <ol style={ulStyle}>
                <li style={color}>Понедельник</li>
                <li style={color1}>Вторник</li>
                <li style={color2}>Среда</li>
                <li style={color}>Четверг</li>
                <li style={color1}>Пятница</li>
                <li style={color2}>Субота</li>
                <li style={color}>Воскресенье</li>
            </ol>
        </div>)
};


const Sign = function () {
    return (
        <div>
            <h3>Знаки зодиака :</h3>
            <div style={ulStyle}>
                <div style={color}>&#9809; Козерог (22 декабря – 19 января)</div>
                <div style={color1}>&#9810; Водолей (20 января – 18 февраля)</div>
                <div style={color2}>&#9811; Рыбы (19 февраля – 20 марта)</div>
                <div style={color}>&#9800; Овен (21 марта – 19 апреля)</div>
                <div style={color1}>&#9801; Телец (21 апреля – 20 мая)</div>
                <div style={color2}>&#9802; Близнецы (21 мая – 20 июня)</div>
                <div style={color}>&#9803; Рак (21 июня – 22 июля)</div>
                <div style={color1}>&#9804;Лев (23 июля – 22 августа)</div>
                <div style={color2}>&#9805; Дева (23 августа – 22 сентября)</div>
                <div style={color}>&#9806; Весы (23 сентября – 22 октября)</div>
                <div style={color1}>&#9807; Скорпион (23 октября – 21 ноября)</div>
                <div style={color2}>&#9808; Стрелец (22 ноября – 21 декабря)</div>
            </div>
        </div>

    )
}





ReactDom.render(<App></App>, document.querySelector("#root"));
