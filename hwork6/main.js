//Class work

/*Создайте функцию-конструктор Calculator, который создаёт объекты с тремя методами: <br>
            read() запрашивает два значения при помощи prompt и сохраняет их значение в свойствах объекта. <br>
            sum() возвращает сумму введённых свойств. <br>
            mul() возвращает произведение введённых свойств. <br>*/

function Calculator() {

    this.read = function () {
        this.value1 = parseFloat(prompt(`Введите первое значение`)),
            this.value2 = parseFloat(prompt(`Введите второе значение`));
    }

    this.sum = function () {
        return this.value1 + this.value2;
    };

    this.mul = function () {
        return this.value1 * this.value2;
    };

};


let rez = new Calculator();
rez.read();

document.getElementById(`a1`).innerHTML = (`Сумма чисел = ${rez.sum()} <br> Умножение чисел = ${rez.mul()} `)


// Home work;

/*Разработайте функцию-конструктор, которая будет создавать обьект Human (человек). 
Создайте массив обьектов и реализуйте функцию, 
которая будет сортировать элементы массива по значению свойства Age по возрастанию или по убыванию.*/

function Human(name, lastName, age, gender, status) {
    this.name = name;
    this.lastName = lastName;
    this.age = age;
    this.gender = gender;
    this.status = status;
};


let humans = [
    new Human("Елена", "Гилберт", 17, "woman", "human"),
    new Human("Кэролайн", "Форбс", 17, "woman", "human"),
    new Human("Аларик", "Зальцман", 31, "man", "human"),
    new Human("Дэймон", "Сальваторе", 156, "man", "vampire"),
    new Human("Ребекка", "Майклсон", 1089, "woman", "vampire"),
    new Human ("Клаус", "Майклсон", 1093, "man", "vampire")
];


function sortage(arr) {

    arr.sort(function (a, b) {
        if (a.age < b.age) {
            return 1;
        }
        if (a.age > b.age) {
            return -1;
        }
        return 0;
    });
}

sortage(humans);
console.dir(humans);


//  Разработайте функцию-конструктор, которая будет создавать обьект Human (человек),
// добавьте на свое усмотрение свойства и методы в этот обьект. 
// Подумайте, какие методы и свойства следует сделать уровня экземпляра, а какие уровня функции-конструктора

let a = prompt(`Выберите супергероя: 1 - Batman, 2 - Spider-man, 3 -  Logan `, '1, 2  или 3')

function Humun(name, weapon, city) {
    this.x = name;
    this.y = weapon;
    this.z = city;

    this.info = function () {
       return `Меня зовут: ${this.x}. <br> Моя суперспособность: ${this.y}. <br>`;
    }
}


Humun.language = "english";

Humun.prototype.all = function () {
   return `Я защищаю ${this.z}`;
}


const person =
    [
        new Humun(``),
        new Humun(`Batman`, `bet_bombs`, `Gotham_City`),
        new Humun(`Spider-man`, `web`, `New_York`),
        new Humun(`Logan`, `regeneration`, `Tokio`)
    ];

document.getElementById(`a3`).innerHTML = (`${person[a].info()}  ${person[a].all()}`)








