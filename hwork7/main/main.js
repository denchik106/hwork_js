// 1 Задание

// Создаем массив со словами
const words = [
    "программа",
    "макака",
    "прекрасный",
    "оладушек"
];
// Выбираем случайное слово
const word = words[Math.floor(Math.random() * words.length)];
// Создаем итоговый массив
const answerArray = [];
for (let i = 0; i < word.length; i++) {
    answerArray[i] = "_";
}
let remainingLetters = word.length;
// Игровой цикл
while (remainingLetters > 0) {
    // Показываем состояние игры
    alert(answerArray.join(" "));
    // 7. Пишем игру «Виселица» 123
    // Запрашиваем вариант ответа
    let guess = prompt(`Угадайте букву, или нажмите Отмена для
выхода из игры.`);
    if (guess === null) {
        // Выходим из игрового цикла
        break;
    } else if (guess.length !== 1) {
        alert("Пожалуйста, введите одиночную букву.");
    } else {
        // Обновляем состояние игры
        for (let j = 0; j < word.length; j++) {
            if (word[j] === guess) {
                answerArray[j] = guess;
                remainingLetters--;
            }
        }
    }
    // Конец игрового цикла
}
// Отображаем ответ и поздравляем игрока
alert(answerArray.join(" "));
document.getElementById(`a1`).innerHTML = (`${word}`);


//2 задание

function createNewUser1() {

    this.name = prompt('Введите ваше имя');
    this.lastName = prompt('Введите фамилию');
    this.getLogin1 = function () {
        let newLogin1 = this.name.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return newLogin1;
    }
}

let newUser1 = new createNewUser1();


document.getElementById(`a2`).innerHTML = (`${newUser1.name} <br> ${newUser1.lastName} <br> Ваш пароль:  ${newUser1.getLogin1()}`);


// 3 Задание

function createNewUser() {

    this.name = prompt('Введите ваше имя');
    this.lastName = prompt('Введите фамилию');
    this.birthday = new Date(prompt(`dd.mm.yyyy`));

    this.getPassword = function () {
        let newPassword = this.name.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();;
        return newPassword
    };

    this.getAge = function () {
        let nowDate = new Date();
        let age = nowDate.getFullYear() - this.birthday.getFullYear();
        return age;
    };

    this.getLogin = function () {
        let newLogin = this.name.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    };
};

let newUser = new createNewUser();

document.getElementById(`a3`).innerHTML = (`Имя: ${newUser.name} <br> Фамилия: ${newUser.lastName} <br> Ваш возраст: ${newUser.getAge()} <br> Ваш пароль: ${newUser.getPassword()}`);


//4 Задание

let arr = ['hello', 'world', 23, '23', null]

const filterBy = (arr, type) => arr.filter(item => typeof item !== type)

document.getElementById(`a4`).innerHTML = (filterBy(arr, "number").join(` `));