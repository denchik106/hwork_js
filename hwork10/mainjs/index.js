/*В папке calculator дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.<br>
* При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.<br>
* При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит - программа ждет введения второго числа для выполнения операции.<br>
* Если пользователь ввел одно число, выбрал оператор, и ввел второе число, то при нажатии как кнопки `=`, так и любого из операторов, в табло должен появиться результат выполенния предыдущего выражения.<br>
* При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву `m` - это значит, что в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране. Повторное нажатие `MRC` должно очищать память.*/

window.addEventListener("DOMContentLoaded", () => {

   let btn = document.querySelector(".keys"),
        display = document.querySelector(".display > input"),
        value1 = "",
        value2 = "",
        sign = "",
        memory = "",
        finish = false;

const digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."],
action = ["/", "+", "-", "*"],
memories = ["mrc", "m-", "m+"];
        

btn.addEventListener("click", (e) => {
    // Если не нажата кнопка
    if (!e.target.classList.contains("button")) return;
    // Если нажата кнопка Clear
    if (e.target.classList.contains("c")) {
    value1 = "";
    value2 = "";
    sign = "";
    display.value = "";
    }

    display.textContent = "";

    // Получаю нажатую кнопку

    let key = e.target.value;

    //Условие для значенией


    
   if(digits.includes(key)){
         if(value2 === "" && sign === ""){
         if(key === "0" && value1 === ""){
            value1 = "0";
     return display.value = "0";
 } else {     
    value1+=key;
    display.value = value1
};

    }else if(finish){
        if(key === "0"){
            value2 = "0";
            return display.value = "0";
        }else
         finish = false;
         value2 = key;
         display.value = value2;
    }else{
        if(key === "0" && value2 === ""){
            value2 = "0";
            return display.value = "0";
           }else{
    display.value = "";   
    value2+=key;
    display.value = value2;
    
   } };
  

} ;

//Условие для знаков

if(action.includes(key)){
        sign = key;
};

//Условие для памяти

if(memories.includes(key)){
    if(key === "m+"){
    display.placeholder = "m";
    memory += display.value;
    }else if( key === "m-"){
    display.placeholder = "m"; 
    memory -= display.value
    }else if(key === "mrc"){
        if(display.value !== memory){
            display.value = "";
            display.value = memory;  
        } else{
            display.placeholder = "";
            memory = "";
            display.value = "";
        }
     
    }

}

   // условие для равно

    if(key == "="){  
    if (value2 === ""){
    value2 = value1;
    }
    switch (sign) {
            case "+": value1 = (+value1) + (+value2)   
                break;
            case "-": value1 = (+value1) - (+value2)
                break;
            case "*": value1 = (+value1) * (+value2)
                break;
            case "/": 
            if(value2 === "0"){
                display.value = "Ошибка"
                value1 = "";
                value2 = "";
                sign = "";
                return;
            } else{
            value1 = (+value1) / (+value2)
                break;
                };
        };

        finish = true;
        display.value = value1;
  
    }
})
})



