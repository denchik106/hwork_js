import React from "react";


function Table(props) {
    return (
        <table>
            <tbody>
                <tr>
                    <th>
                        Номер элемента
                    </th>
                    <th>
                      Название валюты
                    </th>
                    <th>
                        Стоимость валюты
                    </th>
                </tr>
                {props.data.map((element) => {
          return (
            <tr>
              <td>{element.r030}</td>
              <td>{element.txt}</td>
              <td>{element.rate}</td>
            </tr>
          );
        })}
            </tbody>
        </table>
    )
}

export default Table;