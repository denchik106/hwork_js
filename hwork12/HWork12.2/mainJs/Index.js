let price = document.querySelector(`.price>p`),
    sauces = document.querySelector(`.sauces>p`),
    topings = document.querySelector(`.topings>p`),
    rezSauces = 0,
    rezTopings = 0;


//Выбор размера пиццы
const [...a] = document.getElementsByTagName(`form`)[0];

a.forEach((el) => {
    el.addEventListener(`click`, (e) => {
        if (e.target.id === `small`) {
            price.innerHTML = `ЦIНА: 150 грн`
        }
        if (e.target.id === `mid`) {
            price.innerHTML = `ЦIНА: 250 грн`
        }
        if (e.target.id === `big`) {
            price.innerHTML = `ЦIНА: 350 грн`
        }
    })
})


//Перетаскивание элементов

const [...sours] = document.querySelectorAll(".draggable");
const target = document.getElementById("target");


let key = "";
let srcIngredients = document.querySelector('.ingridients');
srcIngredients.addEventListener('mousedown', e => {
    key = e.target.id;
});

sours.forEach(function (el) {
    el.addEventListener("dragstart", function (evt) {
        evt.dataTransfer.effectAllowed = "move";
        evt.dataTransfer.setData("Text", this.id)
    }, false);

});

target.addEventListener("dragover", function (event) {
    if (event.preventDefault) event.preventDefault();
    return false;
}, false);

target.addEventListener("drop", function (event) {
    if (event.preventDefault) event.preventDefault();
    if (event.stopPropagation) event.stopPropagation();

    this.style.border = "";
    let div = document.createElement("img")
    this.appendChild(div);
    if (key === 'sauceClassic') {
        div.src = "./Pizza_pictures/sous-klassicheskij_1557758736353.png"
        rezSauces += 5;
        sauces.innerHTML = ` СОУСИ: ${rezSauces} грн`
    }
    if (key === 'sauceBBQ') {
        div.src = "./Pizza_pictures/sous-bbq_155679418013.png"
        rezSauces += 7;
        sauces.innerHTML = ` СОУСИ: ${rezSauces} грн`

    }
    if (key === 'sauceRikotta') {
        div.src = "./Pizza_pictures/sous-rikotta_1556623391103.png"
        rezSauces += 10;
        sauces.innerHTML = ` СОУСИ: ${rezSauces} грн`

    }
    if (key === 'moc1') {
        div.src = "./Pizza_pictures/mocarela_1556623220308.png"
        rezTopings += 10;
        topings.innerHTML = ` ТОПIНГИ: ${rezTopings} грн`
    }
    if (key === 'moc2') {
        div.src = "./Pizza_pictures/mocarela_1556785182818.png"
        rezTopings += 12;
        topings.innerHTML = ` ТОПIНГИ: ${rezTopings} грн`

    }
    if (key === 'moc3') {
        div.src = "./Pizza_pictures/mocarela_1556785198489.png"
        rezTopings += 15;
        topings.innerHTML = ` ТОПIНГИ: ${rezTopings} грн`

    }
    if (key === "telya") {
        div.src = "./Pizza_pictures/telyatina_1556624025747.png"
        rezTopings += 20;
        topings.innerHTML = ` ТОПIНГИ: ${rezTopings} грн`

    }
    if (key === "vetch1") {
        div.src = "./Pizza_pictures/vetchina.png"
        rezTopings += 10;
        topings.innerHTML = ` ТОПIНГИ: ${rezTopings} грн`

    }
    if (key === "vetch2") {
        div.src = "./Pizza_pictures/vetchina_1556623556129.png"
        rezTopings += 15;
        topings.innerHTML = ` ТОПIНГИ: ${rezTopings} грн`

    }
    return false;
}, false);


//Бегающая кнопка

let speedButton = document.querySelector("#banner");

function random() {
    let rez = Math.floor(Math.random() * 90);
    return rez;
}

speedButton.addEventListener('mouseenter', () => {
    speedButton.position = "absolute";
    speedButton.style.right = `${random()}%`;
    speedButton.style.bottom = `${random()}%`;
});

const [...input] = document.querySelectorAll("input");

//Валидация формы

const [...inputs] = document.getElementsByTagName("form")[1];

localStorage.user = JSON.stringify([]);

class User {
    constructor(name, phone, email) {
        this.name = name;
        this.phone = phone;
        this.email = email;
    }
}

let inputArr = inputs.map((el) => {
    return el;
}).filter((el) => {
    return (el.type != "button" && el.type != "reset");
})


const validate = (target) => {
    switch (target.id) {
        case "name": return /^([а-я]{3,})$/i.test(target.value);
            break;
        case "phone": return /^\+380\d{9}$/.test(target.value);
            break;
        case "email": return /^(([0-9A-z]{1,99})@([A-z]{1,}\.){1,9}[-A-z]{2,})$/.test(target.value);
            break;
        default: throw new Error(`Введите корректное значение`);
    }

}

inputArr.forEach((el) => {
    el.addEventListener("change", function (event) {
        console.log(validate(event.target));
    })
})

let saveButton = document.querySelector("[type=button]");
console.log(saveButton);

saveButton.addEventListener(`click`, () => {
    //Валидация после нажатия кнопки
    const rez = inputArr.map((element) => {
        return validate(element);
    })

    //Проверяем на наличие ошибок
    if (!rez.includes(false)) {

        //если нет ошибок записіваем в локалсторадж 
        let a = JSON.parse(localStorage.user);
        a.push(
            new User(
                ...inputArr.map((el) => {
                    return el.value
                })))

        localStorage.user = JSON.stringify(a);
        inputArr.forEach((e) => {
            e.value = ``;
        })

        location.href='./thank-you.html'
    }
});

