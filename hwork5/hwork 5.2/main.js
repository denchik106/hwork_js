// 2 задание ;
const obj = {
    Коля: '1000',
    Вася: '500',
    Петя: '200'
};
document.getElementById(`a2`).innerHTML = `Коля:  ${obj.Коля} <br/>  Петя:  ${obj.Петя}`;

// 4 задание ;

var names = 'var_text_hello';
var arr = Array.from(names);
var hello = arr.filter(function (number) {
    return number !== "_";
});
document.getElementById(`a4`).innerHTML = (hello.join(``));

// 5 задание ;

let a = parseInt(prompt(`Введите число, больше или меньше 0`));
let ggg = function() {
    if(a>0){
        return ggg = `!`;
    } else{
        return ggg = `!!`;
    }
};

if (a > 0) {
    var ggg1 = function () {
        return ggg1 = `!`;
    }
    ggg1();
    document.getElementById(`a5`).innerHTML = ggg1;
} else {
    var ggg1 = function () {
        return ggg1 = `!!`;
    }
    ggg1();
    document.getElementById(`a5`).innerHTML = ggg1;
}

// 6 задание

// Функция ggg принимает 2 параметра: анонимную функцию, которая возвращает 3 и анонимную функцию, которая
// возвращает 4. Верните результатом функции ggg сумму 3 и 4.


let ggg2 = (a, b) => {
    return a() + b();

};
let anonim1 = () => {
    return 3;
};
let anonim2 = () => {
    return 4;
};
ggg2(anonim1, anonim2);
document.getElementById(`a6`).innerHTML = ggg2(anonim1, anonim2);

// 7 задание

// Сделайте функцию, которая считает и выводит количество своих вызовов.

function counter() {
    var num = 1;

    return function(){
        return num++

    }

}
var rez = counter()

document.getElementById(`a7`).innerHTML = (`${rez()} <br/> ${rez()} <br/> ${rez()}`);


// 8 задание
// Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false.

var object = {};

function isEmpty(obj){
    for(var key in object){
        return false;
    }
    return true;
}

document.getElementById(`a8`).innerHTML=(isEmpty(object));




